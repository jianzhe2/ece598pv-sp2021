use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use rand::Rng;
#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Transaction {
    input: i32,
    output: i32,
    sign: i32,
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let encoded = bincode::serialize(t).unwrap();
    let _signature = key.sign(&encoded[..]);
    _signature
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    let encoded = bincode::serialize(t).unwrap();
    let _public = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, public_key.as_ref());
    _public.verify(&encoded[..], signature.as_ref()).is_ok()
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        // Default::default()
        let input = rand::thread_rng().gen_range(1, 101);
        let output = rand::thread_rng().gen_range(1, 101);
        let sign = rand::thread_rng().gen_range(1, 101);
        let _transaction = Transaction {
            input: input,
            output: output,
            sign: sign,
        };
        println!("{:?}", _transaction);
        _transaction
    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}
