// use std::hash::Hash;
use super::hash::{Hashable, H256};
use ring::digest::{Context, SHA256};

/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    length: usize,
    height: usize,
    nodes: Vec<H256>,
    root: H256,
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        if data.is_empty() {
            let empty_merkle = MerkleTree {
                length: 0,
                height: 0,
                nodes: Vec::new(),
                root: H256::from([0; 32]),
            };
            return empty_merkle;
        }
        // Deal with the size of data.
        let mut new_data = Vec::new();
        let pad = (data.len() as f64).log2().ceil() as u32;
        let new_size = i32::pow(2, pad) as usize;
        let length = new_size;
        let mut i: usize = 0;
        while i < new_size {
            if i < data.len() {
                new_data.push(data[i].hash());
            }
            else {
                new_data.push(data[data.len() - 1].hash());
            }
            i = i + 1;
        }

        // Construct a merkle tree.
        let height = pad + 1;
        let total_nodes_num = (i32::pow(2, height) - 1) as usize;
        let mut nodes = vec![new_data[0]; total_nodes_num];
        
        let mut nodes_index = total_nodes_num - new_data.len();
        let mut data_index = 0;
        while nodes_index < total_nodes_num && data_index < new_size {
            nodes[nodes_index] = new_data[data_index];
            nodes_index += 1;
            data_index += 1;
        }

        let mut remain_index = (total_nodes_num - new_data.len() - 1) as i32;
        while remain_index >= 0 {
            // let left = <[u8; 32]>::from(nodes[(remain_index as usize) * 2 + 1]);
            let left = nodes[(remain_index as usize) * 2 + 1].as_ref();
            // let right = <[u8; 32]>::from(nodes[(remain_index as usize) * 2 + 2]);
            let right = nodes[(remain_index as usize) * 2 + 2].as_ref();
            let mut context = Context::new(&SHA256);
            context.update(&left[..]);
            context.update(&right[..]);
            let buf = context.finish();
            let parent = H256::from(buf);
            nodes[remain_index as usize] = parent;
            remain_index -= 1;
        }

        let root = nodes[0];
        MerkleTree {
            length: length,
            height: height as usize,
            nodes: nodes,
            root: root,
        }
    }

    pub fn root(&self) -> H256 {
        self.root
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        let mut converted_index = index + self.nodes.len() - self.length;
        let mut result = Vec::new();
        let mut level = 1;
        while level < self.height {
            if converted_index % 2 == 0 {
                result.push(self.nodes[converted_index - 1]);
            }
            else {
                result.push(self.nodes[converted_index + 1]);
            }
            converted_index = (converted_index - 1) / 2;
            level += 1;
        }
        result
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut val = *datum;
    let pad = (leaf_size as f64).log2().ceil() as u32;
    let length = (i32::pow(2, pad + 1) - 1) as usize;
    let mut converted_index = index + length - leaf_size;
    let mut count = 0;
    while count < proof.len() {
        let mut context = Context::new(&SHA256);
        if converted_index % 2 == 0 {
            // context.update(&<[u8; 32]>::from(proof[count]));
            // context.update(&<[u8; 32]>::from(val));
            context.update(proof[count].as_ref());
            context.update(val.as_ref());
        }
        else {
            // context.update(&<[u8; 32]>::from(val));
            // context.update(&<[u8; 32]>::from(proof[count]));
            context.update(val.as_ref());
            context.update(proof[count].as_ref());
        }
        val = H256::from(context.finish());
        count += 1;
        converted_index = (converted_index - 1) / 2;
    }
    return val == *root;
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }
}
